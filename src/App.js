import React from 'react';
import Game from './Game';

class App extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {gameNow: false};
    this.startGame = this.startGame.bind(this);
  }

  startGame() {
    this.setState({gameNow: true});
  }




  render() {
    let game = this.state.gameNow ? <Game/> : null;

    return (
        <div>
          <button onClick={this.startGame}>New Game!</button>
          {game}
        </div>
    );
  }
}

export default App;