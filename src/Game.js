import React from 'react';
import Result from './Result';
import './Game.less'


class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {inputNumbers: Array(4)};
    this.handleInputNumber = this.handleInputNumber.bind(this);
  }

  handleInputNumber(event) {
    let eventTargetName = event.target.name;
    let eventTargetValue = event.target.value;
    let numbers = this.state.inputNumbers;

    switch (eventTargetName) {
      case 'firstNumber':
        numbers[0] = eventTargetValue;
        break;
      case 'secondNumber':
        numbers[1] = eventTargetValue;
        break;
      case 'thirdNumber':
        numbers[2] = eventTargetValue;
        break;
      case 'forthNumber':
        numbers[3] = eventTargetValue;
        break;
      default:
        break;
    }

    this.setState({inputNumbers: numbers});
  }

  render() {
    let min = 0;
    let max = 9;
    return (
        <form>
          <fieldset>
            <legend>Input Numbers</legend>
            <input type="number" name="firstNumber" onChange={this.handleInputNumber} min={min} max={max}/>
            <input type="number" name="secondNumber" onChange={this.handleInputNumber} min={min} max={max}/>
            <input type="number" name="thirdNumber" onChange={this.handleInputNumber} min={min} max={max}/>
            <input type="number" name="forthNumber" onChange={this.handleInputNumber} min={min} max={max}/>
          </fieldset>
          <Result/>
          <button type="submit">Good Luck!</button>
        </form>
    );
  }
}

export default Game;
