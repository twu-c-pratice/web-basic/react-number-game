import React from 'react';
import './Result.less';

class Result extends React.Component {
  constructor(props) {
    super(props);
    this.answerList = this.answerList.bind(this);
  }

  render() {
    return (
        <div>
          <h3>Results:</h3>
          <ul>
            {this.answerList()}
          </ul>
        </div>
    );
  }

  answerList() {
    return null;
  }
}

export default Result;